<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20221115103405 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE day (id INT AUTO_INCREMENT NOT NULL, date DATE NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE entry (id INT AUTO_INCREMENT NOT NULL, flatmate_id INT DEFAULT NULL, day_id INT DEFAULT NULL, INDEX IDX_2B219D70CB5CAF22 (flatmate_id), INDEX IDX_2B219D709C24126 (day_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE flatmate (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE entry ADD CONSTRAINT FK_2B219D70CB5CAF22 FOREIGN KEY (flatmate_id) REFERENCES flatmate (id)');
        $this->addSql('ALTER TABLE entry ADD CONSTRAINT FK_2B219D709C24126 FOREIGN KEY (day_id) REFERENCES day (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE entry DROP FOREIGN KEY FK_2B219D70CB5CAF22');
        $this->addSql('ALTER TABLE entry DROP FOREIGN KEY FK_2B219D709C24126');
        $this->addSql('DROP TABLE day');
        $this->addSql('DROP TABLE entry');
        $this->addSql('DROP TABLE flatmate');
    }
}
