<?php

namespace App\Repository;

use App\Entity\Flatmate;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Flatmate>
 *
 * @method Flatmate|null find($id, $lockMode = null, $lockVersion = null)
 * @method Flatmate|null findOneBy(array $criteria, array $orderBy = null)
 * @method Flatmate[]    findAll()
 * @method Flatmate[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FlatmateRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Flatmate::class);
    }

    public function save(Flatmate $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Flatmate $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    /**
     * @return Flatmate[] Returns an array of Flatmate objects
     */
    public function findAll(): array
    {
        $em = $this->getEntityManager();

        $query = $em->createQuery(
          "SELECT f
          FROM App\Entity\Flatmate f
          ORDER BY CASE f.isLivingHere WHEN false THEN 1 ELSE 0 END ASC, f.Name ASC"
        );

        return $query->getResult();
    }

    public function findOneByName($name): ?Flatmate
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.Name = :val')
            ->setParameter('val', $name)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
}
