<?php

namespace App\Controller;

use App\Entity\Day;
use App\Entity\Entry;
use App\Entity\Flatmate;
use App\Repository\DayRepository;
use App\Repository\EntryRepository;
use App\Repository\FlatmateRepository;

use Doctrine\Persistence\ManagerRegistry;

use League\CommonMark\CommonMarkConverter;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class MainController extends AbstractController
{
    #[Route('/', name: 'main')]
    public function main(): Response
    {
        return $this->render('main/index.html.twig');
    }

    #[Route('/api/flatmates', name: 'api_flatmates')]
    public function api_flatmates(FlatmateRepository $fr): JsonResponse
    {
        $data = $fr->findAll();

        $encoders = [new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];
        $serializer = new Serializer($normalizers, $encoders);

        $json = $serializer->normalize($data, 'json');

        return new JsonResponse($json);
    }

    #[Route('/api/update-attendence', name: 'api_update-attendence')]
    public function api_updateAttendence(Request $request, DayRepository $dr, FlatmateRepository $fr, EntryRepository $er, ManagerRegistry $doctrine): Response
    {
        $em = $doctrine->getManager();

        $data = $request->getContent();
        $data = json_decode($data);

        $day = date_create(explode('T', $data->day)[0]);
        $flatmate = $fr->findOneByName($data->flatmate);
        $attendence = $data->attendence;

        $day_obj = $dr->findOneByDate($day);
        
        if($day_obj == null) {
          $day_obj = new Day();
          $day_obj->setDate($day);
          $em->persist($day_obj);
          $em->flush();
        }

        $entry = $er->findOneByFlatmateAndDay($flatmate, $day_obj);

        if($entry != null) {
          $entry->setAttendence($attendence);
        } else {
          $entry = new Entry();
          $entry->setFlatmate($flatmate);
          $entry->setDay($day_obj);
          $entry->setAttendence($attendence);
        }

        $em->persist($entry);
        $em->flush();

        return new Response('OK');
    }

    #[Route('/api/get-attendence', name: 'api_get-attendence')]
    public function api_getAttendence(Request $request, DayRepository $dr, EntryRepository $er): Response
    {
        $data = $request->getContent();
        $data = json_decode($data);

        $day = date_create(explode('T', $data->day)[0]);
        $day_obj = $dr->findOneByDate($day);

        if($day_obj == null) {
          $entries = array();
        } else {
          $entries = $er->findByDay($day_obj);
        }

        $encoders = [new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];
        $serializer = new Serializer($normalizers, $encoders);

        $json = $serializer->normalize($entries, 'json', [
            'circular_reference_handler' => function ($object) {
                return $object->getId();
            }
        ]);

        return new JsonResponse($json);
    }

    #[Route('/api/version', name: 'api_version')]
    public function api_version(): JsonResponse
    {
        $changelog = fopen($this->getParameter('kernel.project_dir') . '/CHANGELOG.md', "r") or die ("Unable to open file!");
        $first_line = fgets($changelog);
        $version = explode('# ', $first_line)[1]; 

        $encoders = [new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];
        $serializer = new Serializer($normalizers, $encoders);

        return new JsonResponse($serializer->normalize($version, 'json'));
    }

    #[Route('/api/changelog', name: 'api_changelog')]
    public function api_changelog(): JsonResponse
    {
        $changelog = fopen($this->getParameter('kernel.project_dir') . '/CHANGELOG.md', "r") or die ("Unable to open file!");
        $text = fread($changelog, filesize($this->getParameter('kernel.project_dir') . '/CHANGELOG.md'));
        fclose($changelog);

        $html = (new CommonMarkConverter())->convert($text)->getContent();

        $encoders = [new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];
        $serializer = new Serializer($normalizers, $encoders);

        return new JsonResponse($serializer->normalize($html, 'json'));
    }

    #[Route('/api/deactivate', name: 'api_deactivate')]
    public function api_deactivate(Request $request, FlatmateRepository $fr, ManagerRegistry $doctrine): Response
    {
        $em = $doctrine->getManager();

        $data = $request->getContent();
        $data = json_decode($data);

        $flatmate = $fr->findOneById($data->flatmate);

        $flatmate->setIsActive(false);
        $em->persist($flatmate);
        $em->flush();

        return new Response('OK');
    }

    #[Route('/api/insert', name: 'api_insert')]
    public function api_insert(Request $request, FlatmateRepository $fr, ManagerRegistry $doctrine): Response
    {
        $em = $doctrine->getManager();

        $data = $request->getContent();
        $data = json_decode($data);

        $name = $data->name;
        $guest = $fr->findOneByName($name);
        if($guest != null && $guest->isIsLivingHere() == true)
          $name = $name . ' 2: Electric Boogaloo';
        $guest = $fr->findOneByName($name);

        if($guest == null) {
          $guest = new Flatmate();
        } 
        $guest->setName($name);
        $guest->setIsActive(true);
        $guest->setIsLivingHere(false);

        $em->persist($guest);
        $em->flush();

        return new Response('OK');
    }
}
