<?php

namespace App\Entity;

use App\Repository\EntryRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: EntryRepository::class)]
class Entry
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\ManyToOne]
    private ?Flatmate $Flatmate = null;

    #[ORM\ManyToOne(inversedBy: 'entries')]
    private ?Day $Day = null;

    #[ORM\Column]
    private ?int $Attendence = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFlatmate(): ?Flatmate
    {
        return $this->Flatmate;
    }

    public function setFlatmate(?Flatmate $Flatmate): self
    {
        $this->Flatmate = $Flatmate;

        return $this;
    }

    public function getDay(): ?Day
    {
        return $this->Day;
    }

    public function setDay(?Day $Day): self
    {
        $this->Day = $Day;

        return $this;
    }

    public function getAttendence(): ?int
    {
        return $this->Attendence;
    }

    public function setAttendence(int $Attendence): self
    {
        $this->Attendence = $Attendence;

        return $this;
    }
}
