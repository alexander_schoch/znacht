import {createTheme, ThemeProvider} from '@mui/material/styles';

const fonts = createTheme({
  typography: {
    fontFamily: [
      'cmu',
    ].join(','),
  },
});

const theme = createTheme({
  typography: {
    fontFamily: [
      'cmu',
    ].join(','),
  },
  palette: {
    type: 'light',
		primary: {
      main: '#02343F',
      light: '#F0EDCC',
      contrastText: '#F0EDCC',
      dark: '#02343F',
    },
    secondary: {
      main: '#02343F',
      light: '#f7c870',
      dark: '#560c0c',
    },
  },
});

export default theme;
