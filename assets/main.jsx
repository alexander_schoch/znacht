import React from 'react';
import ReactDOM from 'react-dom';

import axios from 'axios';

import dayjs from 'dayjs';

import {
  Accordion,
  AccordionSummary,
  AccordionDetails,
  Box,
  Button,
  ButtonGroup,
  Chip,
  Container,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  FormControl,
  Grid,
  IconButton,
  InputLabel,
  List,
  ListItem,
  ListItemButton,
  ListItemIcon,
  ListItemText,
  ListSubheader,
  MenuItem,
  Paper,
  Select,
  TextField,
  ToggleButtonGroup,
  ToggleButton,
  Typography,
} from '@mui/material';

import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { MobileDatePicker } from '@mui/x-date-pickers/MobileDatePicker';

import { ThemeProvider } from '@mui/material/styles';
import theme from './theme';

import ArrowLeftIcon from '@mui/icons-material/ArrowLeft';
import ArrowRightIcon from '@mui/icons-material/ArrowRight';
import CalendarTodayIcon from '@mui/icons-material/CalendarToday';
import ClearIcon from '@mui/icons-material/Clear';
import QuestionMarkIcon from '@mui/icons-material/QuestionMark';
import CheckIcon from '@mui/icons-material/Check';
import HowToVoteIcon from '@mui/icons-material/HowToVote';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import DeleteIcon from '@mui/icons-material/Delete';
import AppleIcon from '@mui/icons-material/Apple';

class Main extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      day: dayjs(),
      flatmates: [],
      flatmate: '',
      selections: [],
      attendence: null,
      rawData: [],
      version: '',
      showChangelog: false,
      changelog: '',
      guest: '',
    }

    this.updateDay = this.updateDay.bind(this);
    this.getFlatmates = this.getFlatmates.bind(this);
    this.getAttendence = this.getAttendence.bind(this);
    this.getVersion = this.getVersion.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.updateAttendence = this.updateAttendence.bind(this);
    this.getByAttendence = this.getByAttendence.bind(this);
    this.getChangelog = this.getChangelog.bind(this);
    this.handleDeactivate = this.handleDeactivate.bind(this);
    this.addGuest = this.addGuest.bind(this);
  }

  componentDidMount() {
    this.getFlatmates();
    this.getAttendence();
    this.getVersion();
    this.getChangelog();
  }

  updateAttendence() {
    let num = 0;
    this.state.rawData.map((entry) => {
      if(entry.Flatmate.name == this.state.flatmate) {
        this.setState({attendence: entry.Attendence});
        num += 1;
      }
    });
    if(num == 0) {
      this.setState({attendence: null});
    }
  }

  getAttendence() {
    axios.post('/api/get-attendence', {
      day: this.state.day,
    }).then(response => {
      this.setState({rawData: response.data}, () => {this.updateAttendence()});
    });
  }

  async getFlatmates() {
    const response = await fetch('/api/flatmates');
    const data = await response.json();
    this.setState({
      flatmates: data,
    });
  }

  async getVersion() {
    const response = await fetch('/api/version');
    const data = await response.json();
    this.setState({
      version: data,
    });
  }

  async getChangelog() {
    const response = await fetch('/api/changelog');
    const data = await response.json();
    this.setState({
      changelog: data,
    });
  }

  handleSubmit(e) {
    if(this.state.flatmate != '') {
      axios.post('/api/update-attendence', {
        attendence: this.state.attendence,
        flatmate: this.state.flatmate,
        day: this.state.day,
      }).then(response => {
        this.getAttendence();
      });
    }
  }

  handleDeactivate(f) {
    axios.post('/api/deactivate', {
      flatmate: f.id,
    }).then(response => {
      this.getFlatmates();
    });
  }

  addGuest(e) {
    e.preventDefault();
    if(this.state.guest != '') {
      axios.post('/api/insert', {
        name: this.state.guest,
      }).then(response => {
        this.getFlatmates();
        this.setState({guest: ''});
      });
    }
  }

  updateDay(by) {
    let newDay = dayjs();
    if(by != 0) {
      newDay = this.state.day.add(by, 'day');
    }
    this.setState({day: newDay}, () => {this.getAttendence()});
  }

  getByAttendence(attendence) {
    if(attendence == -1) {
      let str = '';
      this.state.flatmates.map((flatmate) => {
        let appeared = false;
        this.state.rawData.map((entry) => {
          if(flatmate.Name == entry.Flatmate.name) {
            appeared = true;
          }
        });
        if(!appeared && flatmate.isLivingHere == true) {
          str += flatmate.Name + ', ';
        }
      });
      if(str != '') {
        return str.substring(0, str.length-2);
      } else {
        return str;
      }
    } else {
      let str = '';
      this.state.rawData.map((entry) => {
        if(entry.Attendence == attendence) {
          str += entry.Flatmate.name + ', ';
        }
      });
      if(str != '') {
        return str.substring(0, str.length-2);
      } else {
        return str;
      }
    }
  }

  render () {
    return (
      <ThemeProvider theme={theme}>
        <Container style={{ marginTop: '50px' }}>
          <Typography variant="h3">Znacht <Chip label={this.state.version} onClick={() => {this.setState({showChangelog: true})}}/></Typography>
          <Typography variant="h4" color="secondary">am Holderbachweg 67</Typography>

          <LocalizationProvider dateAdapter={AdapterDayjs}>
            <Paper style={{ padding: '20px', marginTop: '20px' }} elevation={3}>
              <Typography variant="h5">Datum</Typography>
              <Grid container spacing={2} style={{ marginTop: '20px' }}>
                <Grid item xs={12} md={6} style={{ textAlign: 'center' }}>
                  <MobileDatePicker
                    label="Tag"
                    inputFormat="dddd, DD.MM.YYYY"
                    value={this.state.day}
                    onChange={(e) => {this.setState({day: e}, () => {this.getAttendence()})}}
                    renderInput={(params) => <TextField fullWidth {...params} />}
                  />
                </Grid>
                <Grid item xs={12} md={6} style={{ textAlign: 'center' }}>
                  <ButtonGroup variant="contained" aria-label="outlined primary button group">
                    <Button onClick={() => {this.updateDay(-1)}}><ArrowLeftIcon/></Button>
                    <Button onClick={() => {this.updateDay(0)}}><CalendarTodayIcon/></Button>
                    <Button onClick={() => {this.updateDay(1)}}><ArrowRightIcon/></Button>
                  </ButtonGroup>
                </Grid>
              </Grid>
            </Paper>

            <Paper style={{ padding: '20px', marginTop: '20px' }} elevation={3}>
              <Typography variant="h5">Beni ome?</Typography>
              <Grid container spacing={2} style={{ marginTop: '10px' }}>
                <Grid item xs={12} md={6} style={{ textAlign: 'center' }}>
                  <FormControl fullWidth>
                    <InputLabel id="demo-simple-select-label">I bims d/de...</InputLabel>
                    <Select
                      labelId="demo-simple-select-label"
                      id="demo-simple-select"
                      value={this.state.flatmate}
                      label="I bims d/de..."
                      onChange={(e) => {this.setState({flatmate: e.target.value}, () => {this.getAttendence()})}}
                    >
                      <ListSubheader>-- Mitbewohner --</ListSubheader>
                      {this.state.flatmates.map((fm) => {
                        if(fm.isActive == true && fm.isLivingHere == true) {
                          return <MenuItem value={fm.Name} key={fm.id}>{fm.Name}</MenuItem>
                        }
                      })}
                      <ListSubheader>-- Gäst --</ListSubheader>
                      {this.state.flatmates.map((fm) => {
                        if(fm.isActive == true && fm.isLivingHere == false) {
                          return <MenuItem value={fm.Name} key={fm.id}>{fm.Name}</MenuItem>
                        }
                      })}
                    </Select>
                  </FormControl>
                </Grid>
                {this.state.flatmate != '' &&
                  <Grid item xs={12} md={6} style={{ textAlign: 'center' }}>
                    <ToggleButtonGroup
                      value={this.state.attendence}
                      exclusive
                      onChange={(e, selection) => {if(selection !== null) {this.setState({attendence: selection}, () => this.handleSubmit());}}}
                      aria-label="text alignment"
                      variant="contained" 
                    >
                      <ToggleButton value={2} aria-label="yes" color="success">
                        <CheckIcon />
                      </ToggleButton>
                      <ToggleButton value={3} aria-label="leftovers" color="success">
                        <AppleIcon />
                      </ToggleButton>
                      <ToggleButton value={1} aria-label="perhaps">
                        <QuestionMarkIcon />
                      </ToggleButton>
                      <ToggleButton value={0} aria-label="no" color="error">
                        <ClearIcon />
                      </ToggleButton>
                    </ToggleButtonGroup>
                  </Grid>
                }
              </Grid>
            </Paper>

            <Paper style={{ padding: '20px', marginTop: '20px' }} elevation={3}>
              <Typography variant="h5">Wer esch ome?</Typography>
              <Grid container spacing={2} style={{ marginTop: '10px' }}>
                <Grid item xs={12}>
                  <List>
                    <ListItem disablePadding>
                      <ListItemButton>
                        <ListItemIcon>
                          <CheckIcon color="success"/>
                        </ListItemIcon>
                        <ListItemText primary={this.getByAttendence(2)} />
                      </ListItemButton>
                    </ListItem>
                    <ListItem disablePadding>
                      <ListItemButton>
                        <ListItemIcon>
                          <AppleIcon color="success"/>
                        </ListItemIcon>
                        <ListItemText primary={this.getByAttendence(3)} />
                      </ListItemButton>
                    </ListItem>
                    <ListItem disablePadding>
                      <ListItemButton>
                        <ListItemIcon>
                          <QuestionMarkIcon />
                        </ListItemIcon>
                        <ListItemText primary={this.getByAttendence(1)} />
                      </ListItemButton>
                    </ListItem>
                    <ListItem disablePadding>
                      <ListItemButton>
                        <ListItemIcon>
                          <ClearIcon color="error"/>
                        </ListItemIcon>
                        <ListItemText primary={this.getByAttendence(0)} />
                      </ListItemButton>
                    </ListItem>
                    <ListItem disablePadding disabled>
                      <ListItemButton>
                        <ListItemIcon>
                          <HowToVoteIcon />
                        </ListItemIcon>
                        <ListItemText primary={this.getByAttendence(-1)} />
                      </ListItemButton>
                    </ListItem>
                  </List>
                </Grid>
              </Grid>
            </Paper>

            <Accordion style={{ marginTop: '20px' }} elevation={3}>
              <AccordionSummary
                expandIcon={<ExpandMoreIcon/>}
              >
                <Typography variant="h5">Iistöuige</Typography>
              </AccordionSummary>
              <AccordionDetails>
                <Grid container spacing={2}>
                  <Grid item xs={12} md={6}>
                    <Typography variant="h6">Gäst Inetue</Typography>
                    <form>
                    <Grid container spacing={2} style={{ marginTop: '10px' }}>
                      <Grid item xs={9}>
                        <TextField
                          label="Name"
                          value={this.state.guest}
                          onChange={(e) => {this.setState({guest: e.target.value})}}
                          fullWidth
                        />
                      </Grid>
                      <Grid item xs={3}>
                        <Button type="submit" variant="contained" fullWidth style={{ height: "100%" }} onClick={(e) => {this.addGuest(e)}}>
                          Inetue
                        </Button>
                      </Grid>
                    </Grid>
                    </form>
                  </Grid>
                  <Grid item xs={12} md={6}>
                    <Typography variant="h6">Gäst Usenäh</Typography>
                    <List style={{ marginTop: '10px' }}>
                      {this.state.flatmates.map((flatmate) => {
                        if (flatmate.isLivingHere == false && flatmate.isActive == true) {
                          return <ListItem key={flatmate.id} secondaryAction={<IconButton edge="end" onClick={() => {this.handleDeactivate(flatmate)}}><DeleteIcon/></IconButton>}>
                            <ListItemText primary={flatmate.Name}/>
                          </ListItem>
                        }
                      })}
                    </List>
                  </Grid>
                </Grid>
              </AccordionDetails>
            </Accordion>

            <Dialog onClose={() => {this.setState({showChangelog: false})}} open={this.state.showChangelog}>
              <DialogTitle>Changelog</DialogTitle>
              <DialogContent>
                <Typography dangerouslySetInnerHTML={{ __html: this.state.changelog }}></Typography>
              </DialogContent>
              <DialogActions>
                <Button onClick={() => {this.setState({showChangelog: false})}} autoFocus>
                  Close
                </Button>
              </DialogActions>
            </Dialog>
          </LocalizationProvider>
        </Container>
      </ThemeProvider>
    )
  }
}

ReactDOM.render(<Main/>, document.getElementById('main'));
