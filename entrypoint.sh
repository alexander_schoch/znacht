#!/usr/bin/env bash

php bin/console doctrine:migrations:migrate

chmod -R 777 /var/www/znacht/var/cache
chmod -R 777 /var/www/znacht/var/log

nginx
php-fpm
